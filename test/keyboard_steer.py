import numpy as np
import cv2
import pygame
from pygame.locals import *
import socket

class SteerWithScreenTest(object):

    def __init__(self):

        self.stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stream_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with raspberry...")
        self.stream_socket.connect(('192.168.2.100', 8010))
        self.connection = self.stream_socket.makefile('rb')
        print("connected")

        self.control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.control_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with ev3...")
        self.control_socket.connect(('192.168.2.102', 8010))
        print("connected")

        # connect to a seral port
        self.send_inst = True

        # initialise pygame
        pygame.init()
        # apparently one needs to create a display to make pygame work
        self.screen=pygame.display.set_mode((640, 480))
        self.steer()

    def steer(self):

        # collect images for training
        print("Start steering...")

        # stream video frames one by one
        try:
            stream_bytes = ' '
            frame = 1
            while self.send_inst:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    ret, image = cv2.threshold(image,80,255,cv2.THRESH_BINARY)

                    # cv2 pictures are stored in BGR not RGB so we have to fix color before displaying it
                    frame=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
                    # rotate
                    frame=np.rot90(frame,3)
                    frame=np.fliplr(frame)
                    # resize picture to bigger screen
                    frame = cv2.resize(frame,(480,640))
                    #create surface from picture
                    img=pygame.surfarray.make_surface(frame)
                    self.screen.blit(img,(0,0))
                    pygame.display.flip() #update display

                    # get input from human driver
                    for event in pygame.event.get():
                        if event.type == KEYDOWN or event.type == KEYUP:
                            key = pygame.key.get_pressed()

                            # quit if q is pressed on the keyboard
                            if key[K_q]:
                                print("Exit")
                                self.send_inst = False
                                self.control_socket.sendall('q')
                                break
                            elif key[K_p]:
                                print("Stop mindstorm script")
                                self.send_inst = False
                                self.control_socket.sendall('p')
                                break
                            elif key[K_PLUS]:
                                print("Increase wendekreis")
                                self.control_socket.sendall('+')
                            elif key[K_MINUS]:
                                print("Decrease wendekreis")
                                self.control_socket.sendall('-')
                           # if key[K_PLUS]:
                            #    self.contol_socket.sendall('+')
                            #    print 'Speed-Up'
                          #  elif key[K_MINUS]:
                          #      print 'Slowdown'
                          #      self.control_socket.sendall('-')
                            else:
                                control = [key[K_UP], key[K_DOWN], key[K_LEFT], key[K_RIGHT]]
                                print("Control", control)
                                self.control_socket.sendall(''.join(str(e) for e in control))
        finally:
            self.connection.close()
            self.stream_socket.close()
            self.control_socket.close()

if __name__ == '__main__':
    SteerWithScreenTest()
