import numpy as np
import cv2
import socket


class VideoStreamingTest(object):
    def __init__(self):

        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        self.client_socket.connect(('192.168.2.100', 8010))
        self.connection = self.client_socket.makefile('rb')

        self.streaming()

    def streaming(self):

        try:
            print("Connection to raspberry pi")
            print("Streaming...")
            print("Press 'q' to exit")

            stream_bytes = ' '
            while True:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8') # this is how jpeg starts
                last = stream_bytes.find('\xff\xd9')  # this is how jped ends
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    ret, image = cv2.threshold(image,100,255,cv2.THRESH_BINARY)
                    cv2.imshow('image', image)

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
        finally:
            self.connection.close()
            self.client_socket.close()

if __name__ == '__main__':
    VideoStreamingTest()
