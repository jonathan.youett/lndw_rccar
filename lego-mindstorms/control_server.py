import socket
import ev3dev.ev3 as ev3


class ControlServer(object):
    def __init__(self):

        self.motor_left = ev3.LargeMotor('outD')
        self.motor_right = ev3.LargeMotor('outA')

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        self.server_socket.bind(('192.168.2.102', 8010)) #local network for university
        self.server_socket.listen(1)

        self.speed = 40 #60 #30 #50 #60
        self.wende = 0 #25
        self.curve = 40 #60 #50  #90

        self.controlling()

    def forward(self):
       self.motor_left.run_direct(duty_cycle_sp=self.speed)
       self.motor_right.run_direct(duty_cycle_sp=self.speed)

    def back(self):
       self.motor_left.run_direct(duty_cycle_sp=-self.speed)
       self.motor_right.run_direct(duty_cycle_sp=-self.speed)

    def forward_left(self):
       self.motor_left.run_direct( duty_cycle_sp=self.wende)
       self.motor_right.run_direct( duty_cycle_sp=self.curve)

    def forward_right(self):
       self.motor_left.run_direct( duty_cycle_sp=self.curve)
       self.motor_right.run_direct( duty_cycle_sp=self.wende)

    def backward_left(self):
       self.motor_left.run_direct( duty_cycle_sp=-self.wende)
       self.motor_right.run_direct( duty_cycle_sp=-self.curve)

    def backward_right(self):
       self.motor_left.run_direct( duty_cycle_sp=-self.curve)
       self.motor_right.run_direct( duty_cycle_sp=-self.wende)

    def stop(self):
       self.motor_left.run_direct( duty_cycle_sp=0)
       self.motor_right.run_direct( duty_cycle_sp=-0)

    def controlling(self):

        run = True
        try:
            while run:

                print("Waiting for connection...connect and press 'p' to stop mindstorm.\n")
                steer_connection = self.server_socket.accept()[0]

                print("Connected...press 'q' to exit\n")

                try:
                    while True:
                        data = steer_connection.recv(4)
                        if data:
                          data = data.decode("utf-8")
                          if data[0] == 'q':
                            break
                          elif data[0] == 'p':
                            run = False
                            break
                          elif data[0] == '-':
                            self.wende = max(self.wende-5, -100)
                          elif data[0] == '+':
                            self.wende = min(self.wende+5, 100)
                          elif data[0] == '1' and not (data[1] == '1' or data[2] == '1' or data[3] == '1' ):
                            self.forward()
                          elif data[1]  == '1' and not (data[0] == '1'  or data[2] == '1'  or data[3] == '1' ):
                            self.back()
                          elif (data[0] == '1'  and data[2] == '1' ) and not (data[1] == '1'  or data[3] == '1' ):
                            self.forward_left()
                          elif (data[0] == '1'  and data[3] == '1' ) and not (data[1] == '1'  or data[2] == '1' ):
                            self.forward_right()
                          elif (data[1] == '1'  and data[2] == '1' ) and not (data[0] == '1'  or data[3] == '1' ):
                            self.backward_left()
                          elif (data[1] == '1'  and data[3] == '1' ) and not (data[0] == '1'  or data[2] == '1' ):
                            self.backward_right()
                          else:
                            self.stop()
                finally:
                    print("Bye bye\n")
                    self.stop()
                    steer_connection.close()
        finally:
           print("Shutting Down Car\n")
           self.server_socket.close()

if __name__ == '__main__':
  ControlServer()
