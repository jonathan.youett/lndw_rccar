import numpy as np
import cv2
import pygame
from pygame.locals import *
import socket


class CollectTrainingData(object):

    def __init__(self):

        self.stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stream_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with raspberry...")
        self.stream_socket.connect(('192.168.2.100', 8010))
        self.connection = self.stream_socket.makefile('rb')

        self.control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.control_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with ev3...")
        self.control_socket.connect(('192.168.2.102', 8010))

        self.send_inst = True

        # create labels
        self.k = np.zeros((3, 3), 'float')
        for i in range(3):
            self.k[i, i] = 1

        pygame.init()
        self.screen=pygame.display.set_mode((640, 480))
        #self.screen=pygame.display.set_mode((160,120))#,0,24)
        self.collect_image()

    def collect_image(self):

        saved_frame = 0
        total_frame = 0

        # collect images for training
        print("Start collecting images...")
        e1 = cv2.getTickCount()
        #image_array = np.zeros((1, 14400))
        image_array = np.zeros((1, 4800))
        label_array = np.zeros((1, 3), 'float')

        # stream video frames one by one
        try:
            stream_bytes = ' '
            frame = 1
            #store = 0
            while self.send_inst:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    #image = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,3)
                    ret, image = cv2.threshold(image,90,255,cv2.THRESH_BINARY)
                    image = cv2.bitwise_not(image)
                    lowRes = cv2.resize(image, (80,60))

                    # select lower half of the image
                    #roi = image[30:120, :]
                    roi = lowRes#[15:60, :]

                    # save streamed images
                    cv2.imwrite('training_images/frame{:>05}.jpg'.format(frame), roi)

                    # cv2 pictures are stored in BGR not RGB so we have to fix color before displaying it
                    image=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)

                    # rotate
                    image=np.rot90(image,3)
                    image=np.fliplr(image)

                    # artificially blow up image
                    image = cv2.resize(image,(480,640))
                    #create surface from picture
                    image=pygame.surfarray.make_surface(image)
                    self.screen.blit(image,(0,0))
                    #this is just for showing the image on the screen
                    pygame.display.flip()

                    # reshape the roi image into one row array
                    #temp_array = roi.reshape(1, 14400).astype(np.float32)
                    temp_array = roi.reshape(1, 4800).astype(np.float32)

                    frame += 1
                    total_frame += 1

                    # get input from human driver
                    event = pygame.event.get()
                    key = pygame.key.get_pressed()

                    # quit if q is pressed on the keyboard
                    if key[K_q]:
                        print("Exit")
                        self.send_inst = False
                        self.control_socket.sendall('q')
                        break
                    elif key[K_p]:
                        print("Stoping mindstorm")
                        self.send_inst = False
                        self.control_socket.sendall('p')
                        break
                    elif key[K_PLUS]:
                        print("Increase wendekreis")
                        self.control_socket.sendall('+')
                    elif key[K_MINUS]:
                        print("Decrease wendekreis")
                        self.control_socket.sendall('-')

                    # parse control
                    control = [key[K_UP], key[K_DOWN], key[K_LEFT], key[K_RIGHT]]
                    print("Control ", control)

                    # complex orders
                    if key[K_UP]:
                        k = 2
                        if key[K_RIGHT]:
                            k = 1
                        elif key[K_LEFT]:
                            k = 0

                       #         store = store+1
                        #        if store == 10:
                         #           store = 0
                        image_array = np.vstack((image_array, temp_array))
                        label_array = np.vstack((label_array, self.k[k]))
                        saved_frame += 1

                    # send control to raspberry
                    self.control_socket.sendall(''.join(str(e) for e in control))
                    print("total",total_frame," save ",saved_frame)

            # save training images and labels
            train = image_array[1:, :]
            train_labels = label_array[1:, :]

            # save training data as a numpy file
            np.savez('training_data/test08.npz', train=train, train_labels=train_labels)

            e2 = cv2.getTickCount()
            # calculate streaming duration
            time0 = (e2 - e1) / cv2.getTickFrequency()
            print("Streaming duration: ", time0)

            print(train.shape)
            print(train_labels.shape)
            print("Total frame:", total_frame)
            print("Saved frame:", saved_frame)
            print("Dropped frame", total_frame - saved_frame)

        finally:
            self.connection.close()
            self.stream_socket.close()
            self.control_socket.close()

if __name__ == '__main__':
    CollectTrainingData()
