from curtsies import Input
import subprocess
import sys

def execute(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1)

    # Poll process for new output until finished
    while True:
        nextline = process.stdout.readline()
        if nextline == '' and process.poll() is not None:
            break
	print(nextline)
        sys.stdout.flush()

    output = process.communicate()[0]
    exitCode = process.returncode

    if (exitCode == 0):
        return output
    else:
        raise ProcessException(command, exitCode, output)

def main():

    with Input(keynames='curses') as input_generator:
        for e in input_generator:
            #print(repr(e), e)
            if e == u'q':
                break
            if e == u'f':
                print("start driving")
                execute('python collect_data_with_screen.py')
                print("driving finished")

            if e == u't':
                print("start training")
                execute('python mlp_training.py')
                print("training finished")

            if e == u's':
                print("start self-driving")
                execute('python rc_driver.py')
                print("self-driving finished")

            if e == u'd':
                print("start demo")
                execute('python rc_driver_demo.py')
                print("demo finished")

            if e == u'w':
                print("start additional training")
                execute('python train_further.py')
                #process = subprocess.Popen('python keine_ahnung.py',  shell=True, stdout=subprocess.PIPE)
                #process.wait()
                print("additional training finished")

            if e == u'h':
                help()

def help():
    print("Fahren und Datensammeln: f")
    print("Netz trainieren: t")
    print("Selbstfahren: s")
    print("Beenden: q")
    print("Demo mit gespeichertem Netz: d")
    print("Weiter trainieren: w")


if __name__ == '__main__':
    help()
    main()
