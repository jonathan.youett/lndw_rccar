import numpy as np
import cv2
import keras

class neural_network(object):
    output_dim = 3 #left, right, forward
    img_dim = 15 #TODO: this should be defined in a global config file!
    def __init__(self, nnodes, NNtype='tf'):
        self.NNtype = NNtype
        self.nnodes = nnodes
        if NNtype=='tf':
            input_image = keras.Input(shape=img_dim)
            x = input_image
            for i, n_dim in enumerate(nnodes):
                x = keras.Dense(n_dim, activation='softplus', name='layer_'+str(i))(x)
            x = keras.Dense(self.output_dim, activation='softmax')
            self.NN = keras.Model(inputs=input_image, outputs = x)

        elif NNtype=='openCV':
            self.NN = cv2.ml.ANN_MLP()
            self.NN.create()
            self.NN.setLayerSizes(np.array(nnodes))
        else:
            print('Invalid neural network type!')

    def load(self, filename):
        if self.NNtype == 'tf':
            self.NN.load_weights(filename)

        elif self.NNtype == 'openCV':
            self.NN.load(filename)

    def save(self, filename):
        if self.NNtype == 'tf':
            self.NN.save_weights(filename)

        elif self.NNtype == 'openCV':
            self.NN.save(filename)

    def train(self, images, labels, loss='categorial_crossentropy', learning_rate=0.0001, batchsize=1024, epochs=1000):
        if self.NNtype == 'tf':
            optimizer = keras.optimizers.adam(lr = learning_rate)
            self.NN.compile(optimizer = optimizer, loss=loss)
            self.NN.fit(images, labels, epochs=epochs, batch_size=batchsize, shuffle=True)

        elif self.NNtype == 'openCV':
            criteria = (cv2.TERM_CRITERIA_COUNT | cv2.TERM_CRITERIA_EPS, 500, 0.0001)  # 0.00000001)
            params = dict(term_crit=criteria,
                          train_method=cv2.ANN_MLP_TRAIN_PARAMS_BACKPROP,
                          bp_dw_scale=0.001,
                          bp_moment_scale=0.0)
            num_iter = self.NN.train(images, labels, None, params=params)

    def predict(self, image):
        prediction = self.NN.predict(image)
        if self.NNtype == 'tf':
            return prediction.argmax(-1)

        elif self.NNtype == 'openCV':
            return prediction[1].argmax(-1)