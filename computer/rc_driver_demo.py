import threading
import SocketServer
import cv2
import numpy as np
import math
import socket

class NeuralNetwork(object):

    def __init__(self):
        self.model = cv2.ANN_MLP()

    def create(self):
        layer_size = np.int32([14400, 50, 50, 50, 32, 3])
        self.model.create(layer_size)
        self.model.load('mlp_xml/kind_of_good.xml')

    def predict(self, samples):
        ret, resp = self.model.predict(samples)
        return resp.argmax(-1)


class RCControl(object):

    def __init__(self):

        self.stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stream_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with raspberry")
        self.stream_socket.connect(('192.168.2.100', 8010))
        self.connection = self.stream_socket.makefile('rb')


        self.control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.control_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)

        print("Connect with ev3")
        self.control_socket.connect(('192.168.2.102', 8010))

        # create neural network
        self.model = NeuralNetwork()
        self.model.create()
        self.handle()


    def steer(self, prediction):
        if prediction == 2:
            self.control_socket.sendall('1000')
            print("Forward")
        elif prediction == 0:
            self.control_socket.sendall('1010')
            print("Forward Left")
        elif prediction == 1:
            self.control_socket.sendall('1001')
            print("Forward Right")
        else:
            print("Waiting")
            self.control_socket.sendall('0000')

    def handle(self):

        print("Press 'q' to quit")

        stream_bytes = ' '

        # stream video frames one by one
        try:
            while True:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last+2]
                    stream_bytes = stream_bytes[last+2:]
                    gray = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    #gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
                    ret, gray = cv2.threshold(gray,80,255,cv2.THRESH_BINARY)

                    # lower half of the image
                    half_gray = gray[30:120, :]

                    image = cv2.resize(half_gray,(480,640))
                    cv2.imshow('image', image)

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        print("Exiting")
                        self.control_socket.sendall('q')
                        break

                    # reshape image
                    image_array = half_gray.reshape(1, 14400).astype(np.float32)

                    # neural network makes prediction
                    prediction = self.model.predict(image_array)
                    self.steer(prediction)

        finally:
            self.control_socket.sendall('q')
            self.connection.close()
            self.control_socket.close()
            cv2.destroyAllWindows()

if __name__ == '__main__':
    RCControl()
