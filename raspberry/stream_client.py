import io
import sys
import socket
import struct
import time
import picamera

def exception_handler(exception_type, exception, traceback):
    print("Exception found!")

sys.excepthook = exception_handler
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
server_socket.bind(('192.168.2.100', 8010)) #local network for university

server_socket.listen(1)

run = True
try:
    while True:
        print("Waiting for connection...")

        connection = server_socket.accept()[0]
        stream_file = connection.makefile('wb')

        print("connected\n")

        try:
            with picamera.PiCamera() as camera:
                camera.resolution = (160, 120)      # pi camera resolution
               # camera.resolution = (320, 240)      # pi camera resolution
                #camera.resolution = (640, 480)      # pi camera resolution
                camera.framerate = 10               # 10 frames/sec
                stream = io.BytesIO()

                # send jpeg format video stream
                for foo in camera.capture_continuous(stream, 'jpeg', use_video_port = True):
                    stream_file.write(struct.pack('<L', stream.tell()))
                    stream_file.flush()
                    stream.seek(0)
                    stream_file.write(stream.read())

                    stream.seek(0)
                    stream.truncate()
            stream_file.write(struct.pack('<L', 0))
            # detect if connection is closed
        except socket.error, e:
            connection.close()
finally:
    print("Close server")
    client_socket.close()
